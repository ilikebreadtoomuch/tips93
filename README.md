# Tips 93

A Windows 95 like tips window for Windows 93

## Installation

1. Download or clone this repository

2. Put the `tips.js` file from the folder `boot` in `/a/boot/`

3. Make a folder called `tips93` in A: and put `config.json`, `index.html` and `tips.json` there

4. Reboot Windows 93

### Custom Installation Path

If you wish to have a custom installation path edit `tips.js` and replace the `tipspath` variable with your own installation path.

(Ex: `var tipspath = "tips93"` would mean the installation path would be `/a/tips93/`. `var tipspath = "programs/tips93"` would be `/a/programs/tips93/`) 

## Usage

To open this program simply type `tips` in Terminal

If you want to disable the tips windows opening every time you boot up Windows 93, click on the `Show this Welcome Screen next time you start Windows` checkbox or edit `/a/tips93/config.json/` and set `boot` to `false`

To add your own tips, edit `/a/tips93/tips.json`

## Uninstall Tips 93

To uninstall Tips 93 just delete `/a/tips93/` and the `tips.js` file in `/a/boot/`